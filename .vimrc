syntax on                                                                                      
" Show numbers on the side
set number
set ruler
" backgroud setting
set background=dark
" setting how tabs work in vim
set tabstop=4 expandtab

" skeleton files
au BufNewFile *.sh 0r ~/.vim/templates/sh.skeleton | let IndentStyle = "sh"


" XML autocommand 
au Filetype html,xml,xsl source ~/.vim/closetag.vim

" Highlight the cursor line
set cursorline
hi CursorLine cterm=NONE ctermbg=darkblue  ctermfg=white guibg=darkblue guifg=white
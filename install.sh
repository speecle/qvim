#!/bin/sh
# ----------------------------------
# Author : Mohit Narang
# Student: University of Zurich
# E-mail : mohit.narang@uzh.ch, narang.mohit@outlook.com
#------------------------------                               
# This script install the vim settings into a linux/unix with 
# vim configuration in .vimrc file in home directory for a user
# run the script as in the userland and not the root/sudo mode.

# Copy all the directory structure
cp ../qvim/* ~/*
# Copy the .vimrc config file to the home 
cp ../qvim/.vimrc ~/



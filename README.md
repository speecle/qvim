This project contains the settings for vim. 

Installation instructions:

```sh
'Go to your home directory'

'Clone the project'

1. Step 1: git clone https://gitlab.com/speecle/qvim.git

'Change the templates'

2. Step 2: cd .vim/templates/


3. Step 3: edit the templates files using your details

4. Step 4: ./install.sh
```

